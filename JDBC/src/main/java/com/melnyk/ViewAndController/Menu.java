package com.melnyk.ViewAndController;

import com.melnyk.model.AccountEntity;
import com.melnyk.model.LaptopEntity;
import com.melnyk.model.LaptopHasUserEntity;
import com.melnyk.model.PK_LaptopHasUser;
import com.melnyk.model.UserEntity;
import com.melnyk.model.metadata.TableMetaData;
import com.melnyk.persistent.ConnectionManager;
import com.melnyk.service.AccountService;
import com.melnyk.service.LaptopHasUserService;
import com.melnyk.service.LaptopService;
import com.melnyk.service.MetaDataService;
import com.melnyk.service.UserService;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Menu {

  private Map<String, String> menu;
  private Map<String, Printable> menuMethods;
  private static Scanner input = new Scanner(System.in);
  private static Scanner input1 = new Scanner(System.in);

  public Menu() {
    menu = new LinkedHashMap<>();
    menuMethods = new LinkedHashMap<>();
    menu.put("A", "A  - Select all tables");
    menu.put("B", "B  - Select structure of DB");
    menu.put("C", "C  - Select tables names");

    menu.put("1", "1  - Table: Laptop");
    menu.put("11", "11 - Create for Laptop");
    menu.put("12", "12 - Update Laptop");
    menu.put("13", "13 - Delete from Laptop");
    menu.put("14", "14 - Select Laptop");
    menu.put("15", "15 - Find Laptop by ID");
    menu.put("16", "16 - Find Laptop by Model");

    menu.put("2", "2  - Table: Account");
    menu.put("21", "21 - Create for Account");
    menu.put("22", "22 - Update Account");
    menu.put("23", "23 - Delete from Account");
    menu.put("24", "24 - Select Account");
    menu.put("25", "25 - Find Account by ID");

    menu.put("3", "3  - Table: User");
    menu.put("31", "31 - Create for User");
    menu.put("32", "32 - Update User");
    menu.put("33", "33 - Delete from User");
    menu.put("34", "34 - Select User");
    menu.put("35", "35 - Find User by ID");
    menu.put("36", "36 - Find User by Name");

    menu.put("4", "4  - Table: Laptop_has_user");
    menu.put("41", "41 - Create for Laptop_has_user");
    menu.put("43", "42- Delete from Laptop_has_user");
    menu.put("44", "43 - Select Laptop_has_user");
    menu.put("45", "44 - Find all Laptops");
    menu.put("46", "45 - Find all Users");

    menu.put("Q", "Q  - exit");

    menuMethods.put("A", this::selectAllTables);
    menuMethods.put("B", this::takeStructureOfDB);
    menuMethods.put("C", this::selectAllTableNames);

    menuMethods.put("11", this::createLaptop);
    menuMethods.put("12", this::updateLaptop);
    menuMethods.put("13", this::deleteLaptop);
    menuMethods.put("14", this::selectAllLaptops);
    menuMethods.put("15", this::findLaptopByID);
    menuMethods.put("16", this::findLaptopByModel);

    menuMethods.put("21", this::creatAccount);
    menuMethods.put("22", this::updateAccount);
    menuMethods.put("23", this::deletAccount);
    menuMethods.put("24", this::selectAllAccounts);
    menuMethods.put("25", this::findAccountByID);

    menuMethods.put("31", this::createUser);
    menuMethods.put("32", this::updateUser);
    menuMethods.put("33", this::deleteUser);
    menuMethods.put("34", this::selectAllUsers);
    menuMethods.put("35", this::findUserByID);
    menuMethods.put("36", this::findUserByName);

    menuMethods.put("41", this::createLaptopHasUser);
    menuMethods.put("42", this::deleteLaptopHasUser);
    menuMethods.put("43", this::selectAllLaptopHasUser);
    menuMethods.put("44", this::findAllLaptops);
    menuMethods.put("45", this::findAllUsers);
  }

  private void takeStructureOfDB() throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    MetaDataService metaDataService = new MetaDataService();
    List<TableMetaData> structure = metaDataService.getTablesStructure();
    System.out.println("--------Tables: " + connection.getCatalog() + " --------");
    for (TableMetaData tables : structure) {
      System.out.println(tables);
    }
  }

  private void selectAllTableNames() throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    MetaDataService metaDataService = new MetaDataService();
    List<String> structure = metaDataService.findAllTableName();
    System.out.println("--------Tables names: " + connection.getCatalog() + " --------");
    for (String tables : structure) {
      System.out.println(tables);
    }
  }

  private void selectAllTables() throws SQLException {
    selectAllLaptops();
    selectAllAccounts();
    selectAllUsers();
    selectAllLaptopHasUser();
  }

  //--------------------Laptop

  private void findLaptopByModel() throws SQLException {
    System.out.println("Input model for Laptop: ");
    String model = input.nextLine();
    LaptopService laptopService = new LaptopService();
    LaptopEntity laptopEntity = laptopService.findByModel(model);
    System.out.println(laptopEntity);
  }

  private void selectAllLaptops() throws SQLException {
    System.out.println("\nTable: Laptop");
    LaptopService laptopService = new LaptopService();
    List<LaptopEntity> laptopEntities = laptopService.findAll();
    for (LaptopEntity laptopEntity : laptopEntities) {
      System.out.println(laptopEntity);
    }
  }

  private void findLaptopByID() throws SQLException {
    System.out.println("Input id for Laptop: ");
    Integer id = input.nextInt();
    LaptopService laptopService = new LaptopService();
    LaptopEntity laptopEntity = laptopService.findById(id);
    System.out.println(laptopEntity);
  }

  private void createLaptop() throws SQLException {
    System.out.println("Input id for Laptop: ");
    Integer id = input.nextInt();
    System.out.println("Input model for Laptop: ");
    String model = input1.nextLine();
    LaptopEntity laptopEntity = new LaptopEntity(id, model);
    LaptopService laptopService = new LaptopService();
    int count = laptopService.create(laptopEntity);
    System.out.printf("%d row(-s) created", count);
  }

  private void updateLaptop() throws SQLException {
    System.out.println("Input id for Laptop: ");
    Integer id = input.nextInt();
    System.out.println("Input model for Laptop: ");
    String model = input1.nextLine();
    LaptopEntity laptopEntity = new LaptopEntity(id, model);
    LaptopService laptopService = new LaptopService();
    int count = laptopService.update(laptopEntity);
    System.out.printf("%d row(-s) updated", count);
  }

  private void deleteLaptop() throws SQLException {
    System.out.println("Input id for Laptop: ");
    Integer id = input.nextInt();
    LaptopService laptopService = new LaptopService();
    int count = laptopService.delete(id);
    System.out.printf("%d row(-s) deleted", count);
  }

  //--------------------Account

  private void selectAllAccounts() throws SQLException {
    System.out.println("\nTable: Account");
    AccountService accountService = new AccountService();
    List<AccountEntity> accountEntities = accountService.findAll();
    for (AccountEntity accountEntity : accountEntities) {
      System.out.println(accountEntity);
    }
  }

  private void findAccountByID() throws SQLException {
    System.out.println("Input id for Account: ");
    Integer id = input.nextInt();
    AccountService accountService = new AccountService();
    AccountEntity accountEntity = accountService.findById(id);
    System.out.println(accountEntity);
  }

  private void creatAccount() throws SQLException {
    System.out.println("Input id for Account: ");
    Integer id = input.nextInt();
    System.out.println("Input login for Account: ");
    String login = input.nextLine();
    System.out.println("Input password for Account: ");
    String password = input.nextLine();
    System.out.println("Input laptop_id for Account: ");
    Integer laptop_id = input.nextInt();
    AccountEntity accountEntity = new AccountEntity(id, login, password, laptop_id);
    AccountService accountService = new AccountService();
    int count = accountService.create(accountEntity);
    System.out.printf("%d row(-s) created", count);
  }

  private void updateAccount() throws SQLException {
    System.out.println("Input id for Account: ");
    Integer id = input.nextInt();
    System.out.println("Input login for Account: ");
    String login = input.nextLine();
    System.out.println("Input password for Account: ");
    String password = input.nextLine();
    System.out.println("Input laptop_id for Account: ");
    Integer laptop_id = input.nextInt();
    AccountEntity accountEntity = new AccountEntity(id, login, password, laptop_id);
    AccountService accountService = new AccountService();
    int count = accountService.update(accountEntity);
    System.out.printf("%d row(-s) updated", count);
  }

  private void deletAccount() throws SQLException {
    System.out.println("Input id for Account: ");
    Integer id = input.nextInt();
    AccountService accountService = new AccountService();
    int count = accountService.delete(id);
    System.out.printf("%d row(-s) deleted", count);
  }

  //--------------------User

  private void findUserByName() throws SQLException {
    System.out.println("Input name for User: ");
    String name = input.nextLine();
    UserService userService = new UserService();
    UserEntity userEntity = userService.findByName(name);
    System.out.println(userEntity);
  }

  private void selectAllUsers() throws SQLException {
    System.out.println("\nTable: User");
    UserService userService = new UserService();
    List<UserEntity> userEntities = userService.findAll();
    for (UserEntity userEntity : userEntities) {
      System.out.println(userEntity);
    }
  }

  private void findUserByID() throws SQLException {
    System.out.println("Input id for User: ");
    Integer id = input.nextInt();
    UserService userService = new UserService();
    UserEntity userEntity = userService.findById(id);
    System.out.println(userEntity);
  }

  private void createUser() throws SQLException {
    System.out.println("Input id for User: ");
    Integer id = input.nextInt();
    System.out.println("Input name for User: ");
    String name = input.nextLine();
    UserEntity userEntity = new UserEntity(id, name);
    UserService userService = new UserService();
    int count = userService.create(userEntity);
    System.out.printf("%d row(-s) created", count);
  }

  private void updateUser() throws SQLException {
    System.out.println("Input id for User: ");
    Integer id = input.nextInt();
    System.out.println("Input name for User: ");
    String name = input.nextLine();
    UserEntity userEntity = new UserEntity(id, name);
    UserService userService = new UserService();
    int count = userService.update(userEntity);
    System.out.printf("%d row(-s) updated", count);
  }

  private void deleteUser() throws SQLException {
    System.out.println("Input id for User: ");
    Integer id = input.nextInt();
    UserService userService = new UserService();
    int count = userService.delete(id);
    System.out.printf("%d row(-s) deleted", count);
  }

  //--------------------LaptopHasUser

  private void selectAllLaptopHasUser() throws SQLException {
    System.out.println("\n Table laptop_has_user: ");
    LaptopHasUserService hasService = new LaptopHasUserService();
    List<LaptopHasUserEntity> entities = hasService.findAll();
    for (LaptopHasUserEntity entity : entities) {
      System.out.println(entity);
    }
  }

  private void createLaptopHasUser() throws SQLException {
    System.out.println("Input laptop_id for laptop_has_user: ");
    Integer laptopID = input.nextInt();
    System.out.println("Input user_id for laptop_has_user: ");
    Integer userID = input.nextInt();


    LaptopHasUserEntity laptopHasUserEntity = new LaptopHasUserEntity(new PK_LaptopHasUser(laptopID, userID));
    LaptopHasUserService laptopHasUserService = new LaptopHasUserService();
    int count = laptopHasUserService.create(laptopHasUserEntity);
    System.out.printf("%d row(-s) created", count);
  }

  private void findAllLaptops() throws SQLException {
    System.out.println("Input laptop_id to find Laptops: ");
    Integer id = input.nextInt();
    LaptopHasUserService laptopHasUserService = new LaptopHasUserService();
    List<LaptopEntity> all = laptopHasUserService.findAllInFilm(id);
    for (LaptopEntity laptopEntity : all) {
      System.out.println(laptopEntity);
    }
  }

  private void findAllUsers() throws SQLException {
    System.out.println("Input user_id to find Users: ");
    Integer id = input.nextInt();
    LaptopHasUserService laptopHasUserService = new LaptopHasUserService();
    List<UserEntity> all = laptopHasUserService.findAllActorsFilm(id);
    for (UserEntity userEntity : all) {
      System.out.println(userEntity);
    }
  }

  private void deleteLaptopHasUser() throws SQLException {
    System.out.println("Input laptop_id for laptop_has_user: ");
    Integer laptop_id = input.nextInt();
    System.out.println("Input user_id for laptop_has_user: ");
    Integer user_id = input.nextInt();
    PK_LaptopHasUser pk = new PK_LaptopHasUser(laptop_id, user_id);

    LaptopHasUserService laptopHasUserService = new LaptopHasUserService();
    int count = laptopHasUserService.delete(pk);
    System.out.printf("There are deleted %d rows\n", count);
  }

  //--------------------Menu

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String key : menu.keySet())
      if (key.length() == 1) System.out.println(menu.get(key));
  }

  private void outputSubMenu(String fig) {

    System.out.println("\nSubMENU:");
    for (String key : menu.keySet())
      if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();

      if (keyMenu.matches("^\\d")) {
        outputSubMenu(keyMenu);
        System.out.println("Please, select menu point.");
        keyMenu = input.nextLine().toUpperCase();
      }

      try {
        menuMethods.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
