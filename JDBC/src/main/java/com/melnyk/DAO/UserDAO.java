package com.melnyk.DAO;

import com.melnyk.model.UserEntity;
import java.sql.SQLException;

public interface UserDAO extends GeneralDAO<UserEntity, Integer>{

  UserEntity findByName(String name) throws SQLException;
}
