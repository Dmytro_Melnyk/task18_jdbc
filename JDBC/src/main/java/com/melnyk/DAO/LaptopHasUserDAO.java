package com.melnyk.DAO;

import com.melnyk.model.LaptopEntity;
import com.melnyk.model.LaptopHasUserEntity;
import com.melnyk.model.PK_LaptopHasUser;
import com.melnyk.model.UserEntity;
import java.sql.SQLException;
import java.util.List;

public interface LaptopHasUserDAO extends GeneralDAO<LaptopHasUserEntity, PK_LaptopHasUser>{

  List<LaptopEntity> findByLaptopId(Integer laptop_id) throws SQLException;

  List<UserEntity> findByUserId(Integer user_id) throws SQLException;
}
