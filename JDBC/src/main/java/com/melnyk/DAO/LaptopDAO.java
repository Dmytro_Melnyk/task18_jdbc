package com.melnyk.DAO;

import com.melnyk.model.LaptopEntity;
import java.sql.SQLException;
import java.util.List;

public interface LaptopDAO extends GeneralDAO<LaptopEntity, Integer>{

    LaptopEntity findByModel(String model) throws SQLException;
}
