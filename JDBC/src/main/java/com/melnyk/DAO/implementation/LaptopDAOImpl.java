package com.melnyk.DAO.implementation;

import com.melnyk.DAO.LaptopDAO;
import com.melnyk.model.LaptopEntity;
import com.melnyk.persistent.ConnectionManager;
import com.melnyk.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LaptopDAOImpl implements LaptopDAO {

  private static final String GET_ALL = "select * from laptop";
  private static final String GET_BY_ID = "select * from laptop where id=?";
  private static final String GET_BY_MODEL = "select * from laptop where model=?";
  private static final String INSERT = "insert into laptop (model) values (?)";
  private static final String UPDATE = "update laptop set model=? where id=?";
  private static final String DELETE = "delete from laptop where id=?";


  @Override
  public LaptopEntity findByModel(String model) throws SQLException {
    LaptopEntity laptopEntity = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_MODEL)) {
      preparedStatement.setString(1, model);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          laptopEntity = (LaptopEntity) new Transformer(LaptopEntity.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return laptopEntity;
  }

  @Override
  public List<LaptopEntity> findAll() throws SQLException {
    List<LaptopEntity> laptopEntities = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
        while (resultSet.next()) {
          laptopEntities.add((LaptopEntity) new Transformer(LaptopEntity.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return laptopEntities;
  }

  @Override
  public LaptopEntity findById(Integer id) throws SQLException {
    LaptopEntity laptopEntity = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID)) {
      preparedStatement.setInt(1, id);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          laptopEntity = (LaptopEntity) new Transformer(LaptopEntity.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return laptopEntity;
  }

  @Override
  public int create(LaptopEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
      preparedStatement.setString(1, entity.getModel());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int update(LaptopEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
      preparedStatement.setString(1, entity.getModel());
      preparedStatement.setInt(2, entity.getId());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int delete(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
      preparedStatement.setInt(1, id);
      return preparedStatement.executeUpdate();
    }
  }
}
