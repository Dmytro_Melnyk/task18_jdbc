package com.melnyk.DAO.implementation;

import com.melnyk.DAO.UserDAO;
import com.melnyk.model.UserEntity;
import com.melnyk.persistent.ConnectionManager;
import com.melnyk.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {

  private static final String GET_ALL = "select * from user";
  private static final String GET_BY_ID = "select * from user where id=?";
  private static final String GET_BY_NAME = "select * from user where name=?";
  private static final String INSERT = "insert into user (name) values (?)";
  private static final String UPDATE = "update user set name=? where id=?";
  private static final String DELETE = "delete from user where id=?";

  @Override
  public UserEntity findByName(String name) throws SQLException {
    UserEntity userEntity = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_NAME)) {
      preparedStatement.setString(1, name);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          userEntity = (UserEntity) new Transformer(UserEntity.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return userEntity;
  }

  @Override
  public List<UserEntity> findAll() throws SQLException {
    List<UserEntity> userEntities = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
        while (resultSet.next()) {
          userEntities.add((UserEntity) new Transformer(UserEntity.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return userEntities;
  }

  @Override
  public UserEntity findById(Integer id) throws SQLException {
    UserEntity userEntity = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID)) {
      preparedStatement.setInt(1, id);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          userEntity = (UserEntity) new Transformer(UserEntity.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return userEntity;
  }

  @Override
  public int create(UserEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
      preparedStatement.setString(1, entity.getName());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int update(UserEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
      preparedStatement.setString(1, entity.getName());
      preparedStatement.setInt(2, entity.getId());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int delete(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
      preparedStatement.setInt(1, id);
      return preparedStatement.executeUpdate();
    }
  }
}
