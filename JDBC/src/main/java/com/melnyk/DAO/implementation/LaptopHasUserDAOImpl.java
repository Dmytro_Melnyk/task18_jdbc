package com.melnyk.DAO.implementation;

import com.melnyk.DAO.LaptopHasUserDAO;
import com.melnyk.model.LaptopEntity;
import com.melnyk.model.LaptopHasUserEntity;
import com.melnyk.model.PK_LaptopHasUser;
import com.melnyk.model.UserEntity;
import com.melnyk.persistent.ConnectionManager;
import com.melnyk.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LaptopHasUserDAOImpl implements LaptopHasUserDAO {

  private static final String GET_ALL_LAPTOPS = "select id, model from laptop as a join laptop_has_user as b on a.id = b.user_id where laptop_id=?";
  private static final String GET_ALL_USERS = "select id, name from user as a join laptop_has_user as b on a.id = b.laptop_id where user_id=?";
  private static final String GET_ALL = "select * from laptop_has_user";
  private static final String INSERT = "insert into laptop_has_user (laptop_id, user_id) values (?,?)";
  private static final String DELETE = "delete from laptop_has_user where laptop_id=? and user_id=?";

  @Override
  public List<LaptopEntity> findByLaptopId(Integer laptop_id)
      throws SQLException {
    List<LaptopEntity> list = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_LAPTOPS)) {
      preparedStatement.setInt(1, laptop_id);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          list.add((LaptopEntity) new Transformer(LaptopEntity.class).fromResultSetToEntity(resultSet));
        }
        return list;
      }
    }
  }

  @Override
  public List<UserEntity> findByUserId(Integer user_id)
      throws SQLException {
    List<UserEntity> list = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USERS)) {
      preparedStatement.setInt(1, user_id);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          list.add((UserEntity) new Transformer(UserEntity.class).fromResultSetToEntity(resultSet));
        }
        return list;
      }
    }
  }

  @Override
  public List<LaptopHasUserEntity> findAll() throws SQLException {
    List<LaptopHasUserEntity> list = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
        while (resultSet.next()) {
          list.add((LaptopHasUserEntity) new Transformer(LaptopHasUserEntity.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return list;
  }

  @Override
  public LaptopHasUserEntity findById(PK_LaptopHasUser pk_laptopHasUser)
      throws SQLException {
    return null;
  }

  @Override
  public int create(LaptopHasUserEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
      preparedStatement.setInt(1, entity.getPk().getLaptopID());
      preparedStatement.setInt(2, entity.getPk().getUserID());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int update(LaptopHasUserEntity entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(PK_LaptopHasUser pk_laptopHasUser) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
      preparedStatement.setInt(1, pk_laptopHasUser.getLaptopID());
      preparedStatement.setInt(2, pk_laptopHasUser.getUserID());
      return preparedStatement.executeUpdate();
    }
  }
}
