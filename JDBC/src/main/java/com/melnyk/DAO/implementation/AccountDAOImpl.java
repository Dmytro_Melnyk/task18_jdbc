package com.melnyk.DAO.implementation;

import com.melnyk.DAO.AccountDAO;
import com.melnyk.model.AccountEntity;
import com.melnyk.persistent.ConnectionManager;
import com.melnyk.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountDAOImpl implements AccountDAO {

  private static final String GET_ALL = "select * from account";
  private static final String GET_BY_ID = "select * from account where id=?";
  private static final String INSERT = "insert into account (login, password, laptop_id) values (?,?,?)";
  private static final String UPDATE = "update account set login=?, password=?, laptop_id=? where id=?";
  private static final String DELETE = "delete from account where id=?";

  @Override
  public List<AccountEntity> findAll() throws SQLException {
    List<AccountEntity> accounts = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
        while (resultSet.next()) {
          accounts.add((AccountEntity) new Transformer(AccountEntity.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return accounts;
  }

  @Override
  public AccountEntity findById(Integer id) throws SQLException {
    AccountEntity accountEntity = null;
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID)) {
      preparedStatement.setInt(1, id);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          accountEntity = (AccountEntity) new Transformer(AccountEntity.class).fromResultSetToEntity(resultSet);
        }
      }
    }
    return accountEntity;
  }

  @Override
  public int create(AccountEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
      preparedStatement.setString(1, entity.getLogin());
      preparedStatement.setString(2, entity.getPassword());
      preparedStatement.setInt(3, entity.getLaptopID());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int update(AccountEntity entity) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
      preparedStatement.setString(1, entity.getLogin());
      preparedStatement.setString(2, entity.getPassword());
      preparedStatement.setInt(3, entity.getLaptopID());
      preparedStatement.setInt(4, entity.getId());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int delete(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)){
      preparedStatement.setInt(1, id);
      return preparedStatement.executeUpdate();
    }
  }
}
