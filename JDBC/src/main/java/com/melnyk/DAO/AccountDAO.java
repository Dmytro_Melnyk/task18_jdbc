package com.melnyk.DAO;

import com.melnyk.model.AccountEntity;

public interface AccountDAO extends GeneralDAO<AccountEntity, Integer>{

}
