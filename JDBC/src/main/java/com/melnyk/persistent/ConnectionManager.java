package com.melnyk.persistent;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

  private static final String url = "jdbc:mysql://localhost:3306/laptops_db?serverTimezone=UTC&useSSL=false";
  private static final String user = "root";
  private static final String password = "admin";

  private static Connection connection = null;

  public ConnectionManager() {
  }

  public static Connection getConnection() {
    if (connection == null) {
      try {
        connection = DriverManager.getConnection(url, user, password);
      } catch (SQLException e) {
        System.out.println("SQLException: " + e.getMessage());
        System.out.println("SQLState: " + e.getSQLState());
        System.out.println("VendorError: " + e.getErrorCode());
      }
    }
    return connection;
  }
}
