package com.melnyk.service;

import com.melnyk.DAO.implementation.LaptopHasUserDAOImpl;
import com.melnyk.model.LaptopEntity;
import com.melnyk.model.LaptopHasUserEntity;
import com.melnyk.model.PK_LaptopHasUser;
import com.melnyk.model.UserEntity;
import java.sql.SQLException;
import java.util.List;

public class LaptopHasUserService {

  public List<LaptopEntity> findAllInFilm(Integer id) throws SQLException {
    return new LaptopHasUserDAOImpl().findByLaptopId(id);
  }

  public List<UserEntity> findAllActorsFilm(Integer id) throws SQLException {
    return new LaptopHasUserDAOImpl().findByUserId(id);
  }

  public List<LaptopHasUserEntity> findAll() throws SQLException {
    return new LaptopHasUserDAOImpl().findAll();
  }

  public int create(LaptopHasUserEntity filmHasActor) throws SQLException {
    return new LaptopHasUserDAOImpl().create(filmHasActor);
  }

  public int delete(PK_LaptopHasUser pk) throws SQLException {
    return new LaptopHasUserDAOImpl().delete(pk);
  }
}
