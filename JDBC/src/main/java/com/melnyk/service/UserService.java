package com.melnyk.service;

import com.melnyk.DAO.implementation.UserDAOImpl;
import com.melnyk.model.UserEntity;
import java.sql.SQLException;
import java.util.List;

public class UserService {

  public UserEntity findByName(String name) throws SQLException {
    return new UserDAOImpl().findByName(name);
  }

  public List<UserEntity> findAll() throws SQLException {
    return new UserDAOImpl().findAll();
  }

  public UserEntity findById(Integer id) throws SQLException {
    return new UserDAOImpl().findById(id);
  }

  public int create(UserEntity actorEntity) throws SQLException {
    return new UserDAOImpl().create(actorEntity);
  }

  public int update(UserEntity actorEntity) throws SQLException {
    return new UserDAOImpl().update(actorEntity);
  }

  public int delete(Integer id) throws SQLException {
    return new UserDAOImpl().delete(id);
  }
}
