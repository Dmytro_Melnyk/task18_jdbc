package com.melnyk.service;

import com.melnyk.DAO.implementation.AccountDAOImpl;
import com.melnyk.model.AccountEntity;
import java.sql.SQLException;
import java.util.List;

public class AccountService {

  public List<AccountEntity> findAll() throws SQLException {
    return new AccountDAOImpl().findAll();
  }

  public AccountEntity findById(Integer id) throws SQLException {
    return new AccountDAOImpl().findById(id);
  }

  public int create(AccountEntity actorEntity) throws SQLException {
    return new AccountDAOImpl().create(actorEntity);
  }

  public int update(AccountEntity actorEntity) throws SQLException {
    return new AccountDAOImpl().update(actorEntity);
  }

  public int delete(Integer id) throws SQLException {
    return new AccountDAOImpl().delete(id);
  }
}
