package com.melnyk.service;

import com.melnyk.DAO.implementation.LaptopDAOImpl;
import com.melnyk.model.LaptopEntity;
import java.sql.SQLException;
import java.util.List;

public class LaptopService {

  public LaptopEntity findByModel(String model) throws SQLException {
    return new LaptopDAOImpl().findByModel(model);
  }

  public List<LaptopEntity> findAll() throws SQLException {
    return new LaptopDAOImpl().findAll();
  }

  public LaptopEntity findById(Integer id) throws SQLException {
    return new LaptopDAOImpl().findById(id);
  }

  public int create(LaptopEntity actorEntity) throws SQLException {
    return new LaptopDAOImpl().create(actorEntity);
  }

  public int update(LaptopEntity actorEntity) throws SQLException {
    return new LaptopDAOImpl().update(actorEntity);
  }

  public int delete(Integer id) throws SQLException {
    return new LaptopDAOImpl().delete(id);
  }
}
