package com.melnyk.model;

import com.melnyk.model.annotation.Column;
import com.melnyk.model.annotation.PrimaryKey;
import com.melnyk.model.annotation.Table;

@Table(name = "account")
public class AccountEntity {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "login")
  private String login;
  @Column(name = "password")
  private String password;
  @Column(name = "laptop_id")
  private Integer laptopID;

  public AccountEntity() {
  }

  public AccountEntity(Integer id, String login, String password,
      Integer laptopID) {
    this.id = id;
    this.login = login;
    this.password = password;
    this.laptopID = laptopID;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getLaptopID() {
    return laptopID;
  }

  public void setLaptopID(Integer laptopID) {
    this.laptopID = laptopID;
  }

  @Override
  public String toString() {
    return String.format("%-7d %-15s %-15s %s",id, login, password, laptopID);
  }
}
