package com.melnyk.model;

import com.melnyk.model.annotation.Column;
import com.melnyk.model.annotation.PrimaryKey;
import com.melnyk.model.annotation.Table;

@Table(name = "laptop")
public class LaptopEntity {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "model")
  String model;

  public LaptopEntity() {
  }

  public LaptopEntity(Integer id, String model) {
    this.id = id;
    this.model = model;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    return String.format("%-5s %s", id, model);
  }
}
