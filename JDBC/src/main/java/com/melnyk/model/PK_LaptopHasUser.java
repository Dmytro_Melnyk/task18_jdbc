package com.melnyk.model;

import com.melnyk.model.annotation.Column;

public class PK_LaptopHasUser {

  @Column(name = "laptop_id")
  private Integer laptopID;
  @Column(name = "user_id")
  private Integer userID;

  public PK_LaptopHasUser() {
  }

  public PK_LaptopHasUser(Integer laptopID, Integer userID) {
    this.laptopID = laptopID;
    this.userID = userID;
  }

  public Integer getLaptopID() {
    return laptopID;
  }

  public void setLaptopID(Integer laptopID) {
    this.laptopID = laptopID;
  }

  public Integer getUserID() {
    return userID;
  }

  public void setUserID(Integer userID) {
    this.userID = userID;
  }
}
