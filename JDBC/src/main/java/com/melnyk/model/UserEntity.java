package com.melnyk.model;

import com.melnyk.model.annotation.Column;
import com.melnyk.model.annotation.PrimaryKey;
import com.melnyk.model.annotation.Table;

@Table(name = "user")
public class UserEntity {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;

  public UserEntity() {
  }

  public UserEntity(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format("%-5s %s", id, name);
  }
}
