package com.melnyk.model;

import com.melnyk.model.annotation.PrimaryKeyComposite;
import com.melnyk.model.annotation.Table;

@Table(name = "laptop_has_user")
public class LaptopHasUserEntity {

  @PrimaryKeyComposite
  private PK_LaptopHasUser pk;

  public LaptopHasUserEntity() {
  }

  public LaptopHasUserEntity(PK_LaptopHasUser pk) {
    this.pk = pk;
  }

  public PK_LaptopHasUser getPk() {
    return pk;
  }

  public void setPk(PK_LaptopHasUser pk) {
    this.pk = pk;
  }

  @Override
  public String toString() {
    return String.format("%-5d %-5d", pk.getLaptopID(), pk.getUserID());
  }
}
