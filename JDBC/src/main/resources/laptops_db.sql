-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema laptops_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema laptops_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `laptops_db` DEFAULT CHARACTER SET utf8 ;
USE `laptops_db` ;

-- -----------------------------------------------------
-- Table `laptops_db`.`laptop`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `laptops_db`.`laptop` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `model` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laptops_db`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `laptops_db`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laptops_db`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `laptops_db`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `laptop_id` INT NOT NULL,
  PRIMARY KEY (`id`, `laptop_id`),
  INDEX `fk_account_laptop1_idx` (`laptop_id` ASC) VISIBLE,
  CONSTRAINT `fk_account_laptop1`
    FOREIGN KEY (`laptop_id`)
    REFERENCES `laptops_db`.`laptop` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `laptops_db`.`laptop_has_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `laptops_db`.`laptop_has_user` (
  `laptop_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`laptop_id`, `user_id`),
  INDEX `fk_laptop_has_user_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_laptop_has_user_laptop_idx` (`laptop_id` ASC) VISIBLE,
  CONSTRAINT `fk_laptop_has_user_laptop`
    FOREIGN KEY (`laptop_id`)
    REFERENCES `laptops_db`.`laptop` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_laptop_has_user_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `laptops_db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Table `laptops_db`.`user`
-- -----------------------------------------------------
INSERT INTO `laptops_db`.`user` (`id`, `name`) VALUES ('1', 'Dima');
INSERT INTO `laptops_db`.`user` (`id`, `name`) VALUES ('2', 'Vasya');
INSERT INTO `laptops_db`.`user` (`id`, `name`) VALUES ('3', 'Anna');
INSERT INTO `laptops_db`.`user` (`id`, `name`) VALUES ('4', 'Oleg');
INSERT INTO `laptops_db`.`user` (`id`, `name`) VALUES ('5', 'Olena');


-- -----------------------------------------------------
-- Table `laptops_db`.`laptop`
-- -----------------------------------------------------
INSERT INTO `laptops_db`.`laptop` (`id`, `model`) VALUES ('1', 'HP-1');
INSERT INTO `laptops_db`.`laptop` (`id`, `model`) VALUES ('2', 'Lenovo-1');
INSERT INTO `laptops_db`.`laptop` (`id`, `model`) VALUES ('3', 'HP-2');
INSERT INTO `laptops_db`.`laptop` (`id`, `model`) VALUES ('4', 'MAC-1');


-- -----------------------------------------------------
-- Table `laptops_db`.`laptop_has_user`
-- -----------------------------------------------------
INSERT INTO `laptops_db`.`laptop_has_user` (`laptop_id`, `user_id`) VALUES ('1', '1');
INSERT INTO `laptops_db`.`laptop_has_user` (`laptop_id`, `user_id`) VALUES ('1', '2');
INSERT INTO `laptops_db`.`laptop_has_user` (`laptop_id`, `user_id`) VALUES ('2', '4');
INSERT INTO `laptops_db`.`laptop_has_user` (`laptop_id`, `user_id`) VALUES ('3', '3');
INSERT INTO `laptops_db`.`laptop_has_user` (`laptop_id`, `user_id`) VALUES ('4', '5');


-- -----------------------------------------------------
-- Table `laptops_db`.`account`
-- -----------------------------------------------------
INSERT INTO `laptops_db`.`account` (`id`, `login`, `password`, `laptop_id`) VALUES ('1', 'admin', 'admin', '1');
INSERT INTO `laptops_db`.`account` (`id`, `login`, `password`, `laptop_id`) VALUES ('2', 'oleg', 'oleg1998', '2');
INSERT INTO `laptops_db`.`account` (`id`, `login`, `password`, `laptop_id`) VALUES ('3', 'anna', 'anna1996', '3');
INSERT INTO `laptops_db`.`account` (`id`, `login`, `password`, `laptop_id`) VALUES ('4', 'vasya', 'vasya123', '1');
INSERT INTO `laptops_db`.`account` (`id`, `login`, `password`, `laptop_id`) VALUES ('5', 'olena', 'olenaolena', '4');
